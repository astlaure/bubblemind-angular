import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MediaRoutingModule } from './media-routing.module';
import { MediaGridComponent } from './media-grid/media-grid.component';
import { SharedModule } from '../shared/shared.module';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { MediaUploadComponent } from './media-upload/media-upload.component';
import { HttpClientModule } from '@angular/common/http';
import { MediaService } from './media.service';
import { MediaDetailsComponent } from './media-details/media-details.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MediaGridComponent,
    MediaUploadComponent,
    MediaDetailsComponent
  ],
  imports: [
    CommonModule,
    MediaRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule,
    NgxDropzoneModule,
  ],
  providers: [
    MediaService,
  ]
})
export class MediaModule { }
