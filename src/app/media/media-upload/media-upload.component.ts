import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgxDropzoneChangeEvent } from 'ngx-dropzone';
import { MediaService } from '../media.service';

@Component({
  selector: 'app-media-upload',
  templateUrl: './media-upload.component.html',
  styleUrls: ['./media-upload.component.scss']
})
export class MediaUploadComponent implements OnInit {
  @Output() closeUploadEvent = new EventEmitter();

  files: File[] = [];

  constructor(private mediaService: MediaService) { }

  ngOnInit(): void {
  }

  onSelect(event: NgxDropzoneChangeEvent) {
    this.files.push(...event.addedFiles);

    const formData = new FormData();
    formData.append('file', this.files[0]);

    this.mediaService.upload(formData)
      .subscribe(() => this.closeUploadEvent.emit());
  }
}
