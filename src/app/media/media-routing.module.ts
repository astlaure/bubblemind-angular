import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MediaGridComponent } from './media-grid/media-grid.component';
import { MediaUploadComponent } from './media-upload/media-upload.component';

const routes: Routes = [
  { path: '', component: MediaGridComponent },
  { path: 'upload', component: MediaUploadComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MediaRoutingModule { }
