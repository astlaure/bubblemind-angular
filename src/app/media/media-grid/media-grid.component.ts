import { Component, OnInit } from '@angular/core';
import { MediaService } from '../media.service';
import { Observable } from 'rxjs';
import { Media } from '../media.model';

@Component({
  selector: 'app-media-grid',
  templateUrl: './media-grid.component.html',
  styleUrls: ['./media-grid.component.scss']
})
export class MediaGridComponent implements OnInit {
  slug: string = 'saphirdesign';
  modalOpened: boolean = false;
  mediaPage$: Observable<any> = new Observable<any>();

  showDetailsModal: boolean = false;
  selectedMedium?: Media;

  constructor(private mediaService: MediaService) { }

  ngOnInit(): void {
    this.initMedia();
  }

  initMedia() {
    this.mediaPage$ = this.mediaService.getMediaPage();
  }

  toggleModal() {
    this.modalOpened = !this.modalOpened;
  }

  closeModal() {
    this.modalOpened = false;
  }

  uploadSuccessfull() {
    this.closeModal();
    this.initMedia();
  }

  switchDetailsModal(value?: boolean) {
    this.showDetailsModal = value ?? !this.showDetailsModal;
  }

  selectMedium(medium: Media) {
    this.selectedMedium = medium;
    this.switchDetailsModal(true);
  }
}
