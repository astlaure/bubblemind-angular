import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private httpClient: HttpClient) { }

  getMediaPage() {
    return this.httpClient.get('/api/media/saphirdesign');
  }

  upload(formData: FormData) {
    return this.httpClient.post('/api/media/saphirdesign', formData);
  }

  update(id: number, data: any) {
    return this.httpClient.put(`/api/media/saphirdesign/${id}`, data);
  }

  delete(id: number) {
    return this.httpClient.delete(`/api/media/saphirdesign/${id}`);
  }
}
