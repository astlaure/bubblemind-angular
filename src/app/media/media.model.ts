export interface Media {
  id: number;
  uri: string;
  mimetype: string;
  description?: string;
}
