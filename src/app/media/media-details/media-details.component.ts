import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Media } from '../media.model';
import { MediaService } from '../media.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-media-details',
  templateUrl: './media-details.component.html',
  styleUrls: ['./media-details.component.scss']
})
export class MediaDetailsComponent implements OnInit {
  @Input() medium?: Media;
  @Output() closeDetailsEvent = new EventEmitter();

  detailsForm = this.formBuilder.group({
    description: this.formBuilder.control(this.medium?.description),
  });

  constructor(private formBuilder: FormBuilder, private mediaService: MediaService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    //
  }

  editMedium() {
    this.mediaService.update(this.medium!.id, this.detailsForm.value)
      .subscribe(() => this.closeDetailsEvent.emit());
  }

  deleteMedium() {
    this.mediaService.delete(this.medium!.id)
      .subscribe(() => this.closeDetailsEvent.emit());
  }
}
