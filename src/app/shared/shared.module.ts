import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ContentComponent } from './content/content.component';
import { RouterModule } from '@angular/router';
import { CardComponent } from './card/card.component';
import { PageTableLayoutComponent } from './page-table-layout/page-table-layout.component';
import { TinymceEditorComponent } from './tinymce/tinymce-editor/tinymce-editor.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { CoreModule } from '../core/core.module';
import { ModalComponent } from './modal/modal.component';



@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    ContentComponent,
    CardComponent,
    PageTableLayoutComponent,
    TinymceEditorComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    CoreModule,
    EditorModule
  ],
  exports: [
    SidebarComponent,
    ContentComponent,
    CardComponent,
    PageTableLayoutComponent,
    TinymceEditorComponent,
    ModalComponent
  ]
})
export class SharedModule { }
