import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTableLayoutComponent } from './page-table-layout.component';

describe('PageTableLayoutComponent', () => {
  let component: PageTableLayoutComponent;
  let fixture: ComponentFixture<PageTableLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageTableLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTableLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
