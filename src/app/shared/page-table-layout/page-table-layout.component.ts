import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-table-layout',
  templateUrl: './page-table-layout.component.html',
  styleUrls: ['./page-table-layout.component.scss']
})
export class PageTableLayoutComponent implements OnInit {
  @Input() pageTitle: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
