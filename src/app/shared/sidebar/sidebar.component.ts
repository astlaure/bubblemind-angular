import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { map, Observable, Subscriber, Subscription } from 'rxjs';
import { PathParamsService } from '../../core/services/path-params.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  // params$: Observable<string>;
  slug?: string = undefined;
  routeParams: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private pathParamsService: PathParamsService) {
    this.routeParams = this.pathParamsService.routeParams$.subscribe((params) => {
      this.slug = params['slug'];
    });
  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    this.routeParams.unsubscribe();
  }

}
