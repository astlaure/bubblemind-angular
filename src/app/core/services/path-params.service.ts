import { Injectable } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { filter, map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PathParamsService {
  routeParams$: Observable<any>;

  constructor(private router: Router) {
    this.routeParams$ = this.router.events.pipe(
      filter((event) => (event instanceof ActivationEnd)),
      map((event) => event instanceof ActivationEnd ? event.snapshot.params : {}),
    );
  }
}
