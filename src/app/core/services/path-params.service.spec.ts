import { TestBed } from '@angular/core/testing';

import { PathParamsService } from './path-params.service';

describe('PathParamsService', () => {
  let service: PathParamsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PathParamsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
