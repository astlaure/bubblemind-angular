import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebsitesRoutingModule } from './websites-routing.module';
import { WebsiteTableComponent } from './website-table/website-table.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    WebsiteTableComponent
  ],
  imports: [
    CommonModule,
    WebsitesRoutingModule,
    SharedModule,
  ]
})
export class WebsitesModule { }
