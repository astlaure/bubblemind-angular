import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsiteTableComponent } from './website-table.component';

describe('WebsiteTableComponent', () => {
  let component: WebsiteTableComponent;
  let fixture: ComponentFixture<WebsiteTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebsiteTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsiteTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
