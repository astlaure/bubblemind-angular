import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PageCreateComponent } from './page-create/page-create.component';
import { PageTableComponent } from './page-table/page-table.component';


@NgModule({
  declarations: [
    PageTableComponent,
    PageCreateComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
  ]
})
export class PagesModule { }
