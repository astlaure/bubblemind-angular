import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageCreateComponent } from './page-create/page-create.component';
import { PageTableComponent } from './page-table/page-table.component';

const routes: Routes = [
  { path: '', component: PageTableComponent },
  { path: 'create', component: PageCreateComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
