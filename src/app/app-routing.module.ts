import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'admin/websites', pathMatch: 'full',
  },
  {
    path: 'admin/users',
    loadChildren: () => import('./users/users.module').then((mod) => mod.UsersModule),
  },
  {
    path: 'admin/websites',
    loadChildren: () => import('./websites/websites.module').then((mod) => mod.WebsitesModule),
  },
  {
    path: 'website/:slug', redirectTo: 'website/:slug/pages', pathMatch: 'full',
  },
  {
    path: 'website/:slug/users',
    loadChildren: () => import('./users/users.module').then((mod) => mod.UsersModule),
  },
  {
    path: 'website/:slug/media',
    loadChildren: () => import('./media/media.module').then((mod) => mod.MediaModule),
  },
  {
    path: 'website/:slug/pages',
    loadChildren: () => import('./pages/pages.module').then((mod) => mod.PagesModule),
  },
  {
    path: 'website/:slug/templates',
    loadChildren: () => import('./templates/templates.module').then((mod) => mod.TemplatesModule),
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
