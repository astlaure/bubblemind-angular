## Preload the modules

```typescript
RouterModule.forRoot(
  appRoutes,
  {
    preloadingStrategy: PreloadAllModules
  }
)
```

## Can I load the module and then lazy-load the router ...

```typescript

```

## Libraries

1. ngx-awesome-uploader
2. ngx-dropzone
